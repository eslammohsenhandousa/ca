using GACA.EF;
using GACA.EF.Interfaces;
using GACA.EF.Services;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllers();

// ================================================= //
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(
        builder.Configuration.GetConnectionString("DefaultConnection"),
            b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)
    )
);
#region Add Transient
builder.Services.AddTransient<IAuthService, AuthService>();
builder.Services.AddTransient<IAboutService, AboutService>();
builder.Services.AddTransient<IDocumentService, DocumentService>();
builder.Services.AddTransient<IBlogService, BlogService>();
builder.Services.AddTransient<IHomeService, HomeService>();
builder.Services.AddTransient<IMasterPlanService, MasterPlanService>();
builder.Services.AddTransient<IMasterPlanLayerService, MasterPlanLayerService>();
builder.Services.AddTransient<IMediaService, MediaService>();
builder.Services.AddTransient<IMessageService, MessageService>();
builder.Services.AddTransient<IStakeholderService, StakeholderService>();
builder.Services.AddTransient<IProfileService, ProfileService>();
builder.Services.AddTransient<ILinkService, LinkService>();
builder.Services.AddTransient<ISocialService, SocialService>();
builder.Services.AddScoped<IUserService, UserService>();
#endregion
// ================================================= //
#region Configure Swagger UI

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("V1", new OpenApiInfo { Version = "V1", Title = "GACA API", Description = "Main API Documantation of API" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = @"Please provide authorization token to access restricted features.",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = "Bearer",
        BearerFormat = "JWT",
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" } },
            new List<string>()
        }
    });
});
#endregion
#region Configure Bearer Token            
builder.Services.AddJwt(builder.Configuration.GetSection("JWT:Token").Value);

#region Configure Cors
builder.Services.AddCors(o =>
{
    o.AddPolicy(name: "CorsPolicy",
        builder =>
        {
            builder.AllowAnyOrigin()
           .AllowAnyHeader().AllowAnyMethod();
        });
});
#endregion

builder.Services.AddAuthentication();

//builder.Services.AddAuthorization();
#endregion

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI(options => {
        options.SwaggerEndpoint("/swagger/V1/swagger.json", "Main API Documantation of Educal API");
    });
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.UseCors("CorsPolicy");
app.UseStaticFiles();
app.Run();
