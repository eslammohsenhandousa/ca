﻿using GACA.CORE.Dtos.Auth;
using GACA.CORE.Dtos.Auth.ForgetPassword;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<string>> Login(LoginDto dto)
        {
            ResponseResult result = await _authService.ValidateEmployeeUserAsync(dto.EmailAddress, dto.Password);
            if (!result.IsSucceeded)
                return this.StatusCode(result.ApiStatusCode.Value, result);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword(ForgetPasswordDto dto)
        {
            ResponseResult result = await _authService.ForgetPassword(dto);
            if (!result.IsSucceeded)
                return this.StatusCode(result.ApiStatusCode.Value, result);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("ForgetPasswordOTP")]
        public async Task<IActionResult> ForgetPasswordOTP(ForgetPasswordOTPDto dto)
        {
            ResponseResult result = await _authService.ForgetPasswordOTP(dto);
            if (!result.IsSucceeded)
                return this.StatusCode(result.ApiStatusCode.Value, result);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordDto dto)
        {
            ResponseResult result = await _authService.ResetPassword(dto);
            if (!result.IsSucceeded)
                return this.StatusCode(result.ApiStatusCode.Value, result);
            return Ok(result);
        }


    }
}
