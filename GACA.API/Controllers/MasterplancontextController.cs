﻿using GACA.CORE.Dtos.Blog;
using GACA.CORE.Dtos.MasterPlan;
using GACA.CORE.Dtos.MasterPlanLayer;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Services;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MasterplancontextController : ControllerBase
    {

        private readonly IMasterPlanLayerService _masterPlanLayerService;

        public MasterplancontextController(IMasterPlanLayerService masterPlanLayerService)
        {
            _masterPlanLayerService = masterPlanLayerService;
        }

        [HttpGet("GetContextById/{id}")]
        public async Task<IActionResult> GetData(int id)
        {
            ResponseResult result = await _masterPlanLayerService.GetAllByParentId(id);
            return Ok(result);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateMasterPlanLayerDto dto)
        {
            var responseResult = await _masterPlanLayerService.Create(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateMasterPlanLayerDto dto)
        {
            if (dto.Id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _masterPlanLayerService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpDelete("Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _masterPlanLayerService.Delete(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult.ReturnData);
        }
    }
}
