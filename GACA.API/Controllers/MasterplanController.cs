﻿using GACA.CORE.Dtos.MasterPlan;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MasterplanController : ControllerBase
    {

        private readonly IMasterPlanService _masterPlanService;

        public MasterplanController(IMasterPlanService masterPlanService)
        {
            _masterPlanService = masterPlanService;
        }

        [HttpGet("GetData")]
        public async Task<IActionResult> GetData()
        {
            ResponseResult result = await _masterPlanService.GetData();
            return Ok(result);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(MasterPlanDto dto)
        {
            if (dto.Id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _masterPlanService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }
    }
}
