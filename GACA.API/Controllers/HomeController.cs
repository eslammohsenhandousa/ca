﻿using GACA.CORE.Dtos.Home;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class HomeController : ControllerBase
    {

        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        [HttpGet("GetData")]
        public async Task<IActionResult> GetData()
        {
            ResponseResult result = await _homeService.GetData();
            return Ok(result);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(HomeDto dto)
        {
            var responseResult = await _homeService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }
    }
}
