﻿using GACA.CORE.Dtos.Social;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SocialController : ControllerBase
    {

        private readonly ISocialService _socialService;

        public SocialController(ISocialService socialService)
        {
            _socialService = socialService;
        }

        [HttpGet("GetData")]
        public async Task<IActionResult> GetData()
        {
            ResponseResult result = await _socialService.GetData();
            return Ok(result);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(SocialDto dto)
        {
            var responseResult = await _socialService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }
    }
}
