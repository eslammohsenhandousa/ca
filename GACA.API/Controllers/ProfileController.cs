﻿using GACA.CORE.Dtos.Profile;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfileController : ControllerBase
    {

        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [HttpGet("GetMyProfileData")]
        public async Task<IActionResult> GetMyProfileData()
        {
            var jwt = await HttpContext.GetTokenAsync("access_token");
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            var userId = token.Claims.First(x => x.Type == "Id").Value;
            ResponseResult result = await _profileService.GetMyProfile(Convert.ToInt32(userId));
            return Ok(result.ReturnData);
        }

        [HttpPut("UpdateMyProfileData")]
        public async Task<IActionResult> UpdateMyProfileData(ProfileDto dto)
        {
            var jwt = await HttpContext.GetTokenAsync("access_token");
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            var userId = token.Claims.First(x => x.Type == "Id").Value;

            var responseResult = await _profileService.UpdateMyProfile(Convert.ToInt32(userId),dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPut("UpdateMyProfilePassword")]
        public async Task<IActionResult> UpdateMyProfilePassword(UpdateMyPasswordDto dto)
        {
            var jwt = await HttpContext.GetTokenAsync("access_token");
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);
            var userId = token.Claims.First(x => x.Type == "Id").Value;

            var responseResult = await _profileService.UpdateMyPassword(Convert.ToInt32(userId), dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }
    }
}
