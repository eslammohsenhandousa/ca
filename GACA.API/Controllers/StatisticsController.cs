﻿using GACA.CORE.Dtos.About;
using GACA.CORE.Dtos.Landing;
using GACA.CORE.Dtos.Statistics;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StatisticsController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IDocumentService _documentService;
        private readonly IBlogService _blogService;
        private readonly IMessageService _messageService;
        private readonly IStakeholderService _stakeholderService;

        public StatisticsController(IUserService userService, IDocumentService documentService, IBlogService blogService, IMessageService messageService, IStakeholderService stakeholderService)
        {
            _userService = userService;
            _documentService = documentService;
            _blogService = blogService;
            _messageService = messageService;
            _stakeholderService = stakeholderService;
        }

        [HttpGet("GetData")]
        public IActionResult GetData()
        {
            return Ok(new StatisticsDto()
            {
                UserCount = 0,
                DocumentCount = 0,
                BlogCount = 0,
                StakeholderCount = 0,
                MessageCount= 0,
            });
        }
    }
}
