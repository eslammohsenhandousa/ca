﻿using GACA.CORE.Dtos.Blog;
using GACA.CORE.Dtos.Landing;
using GACA.CORE.Dtos.Message;
using GACA.CORE.Dtos.Settings;
using GACA.CORE.Models;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers.Website
{
    [Route("api/landingpage")]
    [ApiController]
    public class LandingpageController : ControllerBase
    {

        private readonly IHomeService _homeService;
        private readonly IAboutService _aboutService;
        private readonly IDocumentService _documentService;
        private readonly IBlogService _blogService;
        private readonly IMessageService _messageService;
        private readonly IStakeholderService _stakeholderService;
        private readonly IMasterPlanService _masterPlanService;
        private readonly IMasterPlanLayerService _masterPlanLayerService;
        private readonly ILinkService _linkService;
        private readonly ISocialService _socialService;

        private readonly IConfiguration _configuration;

        public LandingpageController(IHomeService homeService, 
                IAboutService aboutService, 
                IDocumentService documentService, 
                IBlogService blogService,
                IStakeholderService stakeholderService,
                IMasterPlanService masterPlanService,
                ILinkService linkService,
                ISocialService socialService,
                IMessageService messageService, IConfiguration configuration, IMasterPlanLayerService masterPlanLayerService)
        {
            _homeService = homeService;
            _aboutService = aboutService;
            _documentService = documentService;
            _blogService = blogService;
            _messageService = messageService;
            _stakeholderService = stakeholderService;
            _masterPlanService  =  masterPlanService;
            _configuration = configuration;
            _masterPlanLayerService = masterPlanLayerService;
            _linkService = linkService;
            _socialService = socialService;
        }

        // GET: api/<LandingpageController>
        [HttpGet("homepage")]
        public async Task<IActionResult> GetHomePage()
        {
            ResponseResult hero = await _homeService.GetData();
            ResponseResult masterPlan = await _masterPlanService.GetData();
            ResponseResult news = await _blogService.GetLastNews();
            ResponseResult stakeHolder = await _stakeholderService.GetData();
            return Ok(new LandingDto() { 
                Hero = hero.ReturnData, 
                MasterPlan = masterPlan.ReturnData, 
                News = news.ReturnData,
                StakeHolder = stakeHolder.ReturnData
            });
        }

        // GET: api/<LandingpageController>
        [HttpGet("about")]
        public async Task<IActionResult> GetAbout()
        {
            ResponseResult result = await _aboutService.GetData();
            return Ok(result.ReturnData);
        }

        // GET: api/<LandingpageController>
        [HttpGet("downloads")]
        public async Task<IActionResult> GetDownlods()
        {
            ResponseResult result = await _documentService.GetData();
            return Ok(result.ReturnData);
        }

        // GET: api/<LandingpageController>
        [HttpGet("blogs")]
        public async Task<IActionResult> GetBlogs(int PageNumber, int PageSize)
        {
            ResponseResult result = await _blogService.GetAllPagination(PageNumber, PageSize, null, null, null);
            return Ok(result.ReturnData);
        }

        // GET: api/<LandingpageController>
        [HttpGet("blogs/{id:int}")]
        public async Task<IActionResult> GetSingleBlog(int id)
        {
            var responseResult = await _blogService.GetById(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult.ReturnData);
        }

        // POST api/<LandingpageController>
        [HttpPost("feedback")]
        public async Task<IActionResult> StoreNewMessage(AddMessageDto dto)
        {
            if (!ModelState.IsValid)
            {
                return this.StatusCode(400, "Some Data Required");
            }
            var responseResult = await _messageService.Create(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        // GET: api/<LandingpageController>
        [HttpGet("settings")]
        public async Task<IActionResult> GetSettings()
        {
            var Version = _configuration.GetSection("Settings:Version").Value;
            var Copyright = _configuration.GetSection("Settings:Copyright").Value;
            var Constructor = _configuration.GetSection("Settings:Constructor").Value;
            ResponseResult Links = await _linkService.GetData();
            ResponseResult Social = await _socialService.GetData();
            return Ok(new SettingsDto() { 
                Version = Version, 
                Copyright = Copyright, 
                Constructor = Constructor, 
                Links = Links.ReturnData,
                Social = Social.ReturnData,
                FooterDescription = "Develop the transport system to make the Kingdom a logistics center linking the three continents and promote sustainable economic development and competitiveness adequate to the Saudi Vision 2030",
            });
        }

        // GET: api/<LandingpageController>
        [HttpGet("masterplan")]
        public async Task<IActionResult> GetMasterPlanPage()
        {
            ResponseResult masterPlan = await _masterPlanService.GetMasterPlanAllData();
            return Ok(masterPlan.ReturnData);
        }

        [HttpGet("masterplan/{id:int}")]
        public async Task<IActionResult> GetMasterPlanContext(int id)
        {
            ResponseResult masterPlanLayerContent = await _masterPlanLayerService.GetAllByParentId(id);
            return Ok(masterPlanLayerContent.ReturnData);
        }

        [HttpGet("masterplan/{id:int}/data/{page:int}")]
        public async Task<IActionResult> GetMasterPlanContextData(int id,int page)
        {
            ResponseResult masterPlanLayerContent = await _masterPlanLayerService.GetById(page);
            return Ok(masterPlanLayerContent.ReturnData);
        }
    }
}
