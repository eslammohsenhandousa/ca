﻿using GACA.CORE.Dtos.About;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AboutController : ControllerBase
    {

        private readonly IAboutService _aboutService;

        public AboutController(IAboutService aboutrService)
        {
            _aboutService = aboutrService;
        }

        [HttpGet("GetData")]
        public async Task<IActionResult> GetData()
        {
            ResponseResult result = await _aboutService.GetData();
            return Ok(result);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(AboutDto dto)
        {
            var responseResult = await _aboutService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }
    }
}
