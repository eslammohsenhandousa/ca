﻿using GACA.CORE.Dtos.Media;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MediaController : ControllerBase
    {

        private readonly IMediaService _mediaService;

        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        [HttpPost("UploadFile/MediaType/{mediaType}/Id/{id}")]
        public async Task<IActionResult> UploadFile(IFormFile file ,string mediaType,int id)
        {
            if (file != null && file.Length > 0)
            {
                #region Create Directory
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Uploads");
                //create folder if not exist
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                #endregion

                #region Rename File Or Image

                string extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1] ;
                string fileName = DateTime.Now.Ticks.ToString() + extension;

                string fileNameWithPath = Path.Combine(path, fileName);
                using (var stream = new FileStream(fileNameWithPath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                #endregion
                var dto = new AddMediaDto
                {
                    Id = id,
                    MediaType = mediaType,
                    MediaUrl = "Uploads/" + fileName
                };
                await _mediaService.SaveFilesRelatedToID(dto);
                return Ok(new ResponseResult { ApiStatusCode = (int)ApiStatusCodeEnum.BadRequest, IsSucceeded = true, ReturnData = dto.MediaUrl });
            }
            else
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, new ResponseResult { IsSucceeded = false, ReturnData = false, ErrorMessage = Messages.NoFileFound });
        }
    }
}
