﻿using GACA.CORE.Dtos.Blog;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BlogController : ControllerBase
    {

        private readonly IBlogService _blogService;

        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
        }

        [HttpGet("GetAllPagination")]
        public async Task<IActionResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title)
        {
            ResponseResult result = await _blogService.GetAllPagination( PageNumber,  PageSize, CreateAtFrom,  CreateAtTo, Title);
            return Ok(result.ReturnData);
        }

        [HttpGet("GetById/{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            var responseResult = await _blogService.GetById(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(AddBlogDto dto)
        {
            var responseResult = await _blogService.Create(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateBlogDto dto)
        {
            if (dto.Id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _blogService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpDelete("Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _blogService.Delete(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult.ReturnData);
        }
    }
}
