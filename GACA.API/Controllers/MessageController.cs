﻿using GACA.CORE.Dtos.Stakeholder;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessageController : ControllerBase
    {

        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet("GetAllPagination")]
        public async Task<IActionResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email, string? Phone, string? Type)
        {
            ResponseResult result = await _messageService.GetAllPagination( PageNumber,  PageSize, CreateAtFrom,  CreateAtTo, Name, Email, Phone, Type);
            return Ok(result.ReturnData);
        }

        [HttpGet("GetById/{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            var responseResult = await _messageService.GetById(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpDelete("Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _messageService.Delete(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult.ReturnData);
        }
    }
}
