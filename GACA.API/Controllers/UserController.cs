﻿using GACA.CORE.Dtos.Auth;
using GACA.CORE.Dtos.User;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GACA.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {

        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("GetAllPagination")]
        public async Task<IActionResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email)
        {
            ResponseResult result = await _userService.GetAllPagination( PageNumber,  PageSize, CreateAtFrom,  CreateAtTo,  Name,  Email);
            return Ok(result.ReturnData);
        }

        [HttpGet("GetById/{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            var responseResult = await _userService.GetById(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPost("Create")]
        [AllowAnonymous]
        public async Task<IActionResult> Create(AddUserDto dto)
        {
            var responseResult = await _userService.Create(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UserDtos dto)
        {
            if (dto.Id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _userService.Update(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpPut("UpdatePassword")]
        public async Task<IActionResult> UpdatePassword(UpdatePasswordDto dto)
        {
            var responseResult = await _userService.UpdatePassword(dto);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult);
        }

        [HttpDelete("Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id <= 0)
                return this.StatusCode((int)ApiStatusCodeEnum.BadRequest, Messages.IdentifierRequired);
            var responseResult = await _userService.Delete(id);
            if (!responseResult.IsSucceeded)
                return this.StatusCode(responseResult.ApiStatusCode.Value, responseResult);
            return Ok(responseResult.ReturnData);
        }
    }
}
