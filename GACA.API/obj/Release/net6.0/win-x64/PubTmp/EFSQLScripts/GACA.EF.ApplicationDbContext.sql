﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [About] (
        [Id] int NOT NULL IDENTITY,
        [Content] nvarchar(max) NOT NULL,
        [Vision] nvarchar(max) NOT NULL,
        [Mission] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_About] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Blogs] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [Description] nvarchar(max) NOT NULL,
        [ImageUrl] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        CONSTRAINT [PK_Blogs] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Documents] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [ImageUrl] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        CONSTRAINT [PK_Documents] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Home] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [Description] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Home] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Links] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [LinkAddress] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        CONSTRAINT [PK_Links] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [MaterPlans] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [Descripton] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_MaterPlans] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Messages] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(150) NOT NULL,
        [Email] nvarchar(max) NOT NULL,
        [Phone] nvarchar(max) NOT NULL,
        [FeedBackType] int NOT NULL,
        [FeedBackTitle] nvarchar(150) NOT NULL,
        [FeedBackMessage] nvarchar(max) NOT NULL,
        [IsSeen] bit NOT NULL,
        [IpAddress] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        CONSTRAINT [PK_Messages] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Socials] (
        [Id] int NOT NULL IDENTITY,
        [FaceBook] nvarchar(max) NOT NULL,
        [Twitter] nvarchar(max) NOT NULL,
        [YouTube] nvarchar(max) NOT NULL,
        [Instgram] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Socials] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Stakeholders] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [ImageUrl] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        [DateUpdated] datetime2 NULL,
        CONSTRAINT [PK_Stakeholders] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [Users] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(150) NOT NULL,
        [Email] nvarchar(max) NOT NULL,
        [Password] varbinary(150) NOT NULL,
        [PasswordSalt] varbinary(max) NOT NULL,
        [OTP] nvarchar(max) NOT NULL,
        [Token] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        [DateUpdated] datetime2 NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE TABLE [MasterPlanLayers] (
        [Id] int NOT NULL IDENTITY,
        [Title] nvarchar(150) NOT NULL,
        [Description] nvarchar(max) NOT NULL,
        [DateCreated] datetime2 NOT NULL,
        [DateUpdated] datetime2 NULL,
        [MasterPlanId] int NOT NULL,
        CONSTRAINT [PK_MasterPlanLayers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_MasterPlanLayers_MaterPlans_MasterPlanId] FOREIGN KEY ([MasterPlanId]) REFERENCES [MaterPlans] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Content', N'Mission', N'Vision') AND [object_id] = OBJECT_ID(N'[About]'))
        SET IDENTITY_INSERT [About] ON;
    EXEC(N'INSERT INTO [About] ([Id], [Content], [Mission], [Vision])
    VALUES (1, N''Welcome Content LookUp'', N''Welcome Mission LookUp'', N''Welcome Vision LookUp'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Content', N'Mission', N'Vision') AND [object_id] = OBJECT_ID(N'[About]'))
        SET IDENTITY_INSERT [About] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Description', N'Title') AND [object_id] = OBJECT_ID(N'[Home]'))
        SET IDENTITY_INSERT [Home] ON;
    EXEC(N'INSERT INTO [Home] ([Id], [Description], [Title])
    VALUES (1, N''Welcome Description LookUp'', N''Welcome Title LookUp'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Description', N'Title') AND [object_id] = OBJECT_ID(N'[Home]'))
        SET IDENTITY_INSERT [Home] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descripton', N'Title') AND [object_id] = OBJECT_ID(N'[MaterPlans]'))
        SET IDENTITY_INSERT [MaterPlans] ON;
    EXEC(N'INSERT INTO [MaterPlans] ([Id], [Descripton], [Title])
    VALUES (1, N'''', N''Layer 1''),
    (2, N'''', N''Layer 2''),
    (3, N'''', N''Layer 3''),
    (4, N'''', N''Layer 4''),
    (5, N'''', N''Layer 5'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descripton', N'Title') AND [object_id] = OBJECT_ID(N'[MaterPlans]'))
        SET IDENTITY_INSERT [MaterPlans] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FaceBook', N'Instgram', N'Twitter', N'YouTube') AND [object_id] = OBJECT_ID(N'[Socials]'))
        SET IDENTITY_INSERT [Socials] ON;
    EXEC(N'INSERT INTO [Socials] ([Id], [FaceBook], [Instgram], [Twitter], [YouTube])
    VALUES (1, N''https://facebook.com'', N''https://instagram.com'', N''https://twitter.com'', N''https://youtube.com'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FaceBook', N'Instgram', N'Twitter', N'YouTube') AND [object_id] = OBJECT_ID(N'[Socials]'))
        SET IDENTITY_INSERT [Socials] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'DateCreated', N'DateUpdated', N'Email', N'Name', N'OTP', N'Password', N'PasswordSalt', N'Token') AND [object_id] = OBJECT_ID(N'[Users]'))
        SET IDENTITY_INSERT [Users] ON;
    EXEC(N'INSERT INTO [Users] ([Id], [DateCreated], [DateUpdated], [Email], [Name], [OTP], [Password], [PasswordSalt], [Token])
    VALUES (1, ''2024-01-23T21:01:16.2548844+03:00'', ''2024-01-23T21:01:16.2548854+03:00'', N''admin@admin.com'', N''Admin'', N'''', 0x75534D1CF19A222A3DE79EA8A9AF7FF2EC3D5A1CC7A11B4F9C61D5A65FAD9E2C4F1B6657027A60478F67C1AFB0D555E1ABD26F4A528B53BDFF77788017FA59EF, 0x6B294EF67FCE2EBF76576B8A3FEBFFE650FBA9D3619C5F9B52EE5A7A67484BF430FF3846D2866555387BBDAC19B8632D5D4599A93C3F85D154B1B68FBB1AABA3BC636E9337C9B56BE30E419FD1F4313F250340C0A404DE777AD6C99B0B31AA931116DC3EF95004CA04608DA14BCA8052123AC758F40E22AF3D37FCA292448958, N'''')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'DateCreated', N'DateUpdated', N'Email', N'Name', N'OTP', N'Password', N'PasswordSalt', N'Token') AND [object_id] = OBJECT_ID(N'[Users]'))
        SET IDENTITY_INSERT [Users] OFF;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    CREATE INDEX [IX_MasterPlanLayers_MasterPlanId] ON [MasterPlanLayers] ([MasterPlanId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240123180116_snap-database')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20240123180116_snap-database', N'6.0.25');
END;
GO

COMMIT;
GO

