﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GACA.EF.Shared
{
    public static class SendMailService
    {
        public static IConfiguration AppSetting { get; }

        static SendMailService()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }


        public static bool SendMail(string to, string subject, string body, bool isBodyHtml = false, string bbc = "")
        {
            // Configure mail client (may need additional
            // code for authenticated SMTP servers)
            SmtpClient mailClient = new SmtpClient(AppSetting.GetSection("Mail:Host").Value);
            mailClient.EnableSsl = false;// changed for // Server does not support secure connections.
            mailClient.Port = 587;
            mailClient.Credentials = new System.Net.NetworkCredential(AppSetting.GetSection("Mail:From").Value, AppSetting.GetSection("Mail:Password").Value);

            // Create the mail message
            MailMessage mailMessage = new MailMessage(AppSetting.GetSection("Mail:From").Value, to, subject, body);

            if (bbc.Length > 0)
                mailMessage.Bcc.Add(bbc);
            mailMessage.IsBodyHtml = isBodyHtml;

            try
            {
                mailClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                var r = ex.Message;
                return false;
            }

        }
    }
    // We Get This Data From the DataBase
    // from => that is the Sender Email
    // password => that is the Sender Email Password
    // to => that is the Receiver Email
    // subject => that is the subject Title
    // body => that is the Body of the Email
    // bbc => that is an Empty String
    // host => that is the Host Email

}
