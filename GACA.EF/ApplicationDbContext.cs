﻿using GACA.CORE.Models;
using GACA.EF.Shared;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using System.Text.Json.Nodes;

namespace GACA.EF
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<About>().HasData(new About()
            {
                Id = 1,
                Content = "Welcome Content LookUp",
                Mission = "Welcome Mission LookUp",
                Vision = "Welcome Vision LookUp",
            });
            modelBuilder.Entity<Home>().HasData(new Home()
            {
                Id = 1,
                Title = "Welcome Title LookUp",
                Description = "Welcome Description LookUp",
            });

            modelBuilder.Entity<Social>().HasData(new Social()
            {
                Id = 1,
                FaceBook = "https://facebook.com",
                Twitter = "https://twitter.com",
                Instgram = "https://instagram.com",
                YouTube = "https://youtube.com",
            });

            #region Master Plan
            modelBuilder.Entity<MasterPlan>().HasData(new MasterPlan()
            {
                Id = 1,
                Title = "Layer 1",
            });
            modelBuilder.Entity<MasterPlan>().HasData(new MasterPlan()
            {
                Id = 2,
                Title = "Layer 2",
            });
            modelBuilder.Entity<MasterPlan>().HasData(new MasterPlan()
            {
                Id = 3,
                Title = "Layer 3",
            });
            modelBuilder.Entity<MasterPlan>().HasData(new MasterPlan()
            {
                Id = 4,
                Title = "Layer 4",
            });
            modelBuilder.Entity<MasterPlan>().HasData(new MasterPlan()
            {
                Id = 5,
                Title = "Layer 5",
            });
            #endregion
            #region Create Admin User
            byte[] PasswordHashed, PasswordSalt;
            Auth.GetPasswordHash("123456789", out PasswordHashed, out PasswordSalt);
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = 1,
                Name = "Admin",
                Email = "admin@admin.com",
                Password = PasswordHashed, // 123456789
                PasswordSalt = PasswordSalt
            });
            #endregion

        }

        public DbSet<About> About { get; set; }

        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Document> Documents { get; set; }

        public DbSet<Home> Home { get; set; }

        public DbSet<MasterPlan> MasterPlans { get; set; }

        public DbSet<MasterPlanLayer> MasterPlanLayers { get; set; }

        public DbSet<Link> Links { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Stakeholder> Stakeholders { get; set; }

        public DbSet<Social> Socials { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
