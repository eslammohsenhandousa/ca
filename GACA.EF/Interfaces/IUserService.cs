﻿using GACA.CORE.Dtos.User;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IUserService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email);
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(AddUserDto dto);
        Task<ResponseResult> Update(UserDtos dto);
        Task<ResponseResult> Delete(long id);
        Task<ResponseResult> ChangePassword(ChangePasswordDto dto);
        Task<ResponseResult> UpdatePassword(UpdatePasswordDto dto);
    }
}
