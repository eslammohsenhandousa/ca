﻿using GACA.CORE.Dtos.Social;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface ISocialService
    {
        Task<ResponseResult> GetData();
        Task<ResponseResult> Update(SocialDto dto);
    }
}
