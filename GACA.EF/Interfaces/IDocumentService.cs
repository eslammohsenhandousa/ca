﻿using GACA.CORE.Dtos.Document;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IDocumentService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title);
        Task<ResponseResult> GetData();
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(AddDocumentDto dto);
        Task<ResponseResult> Update(UpdateDocumentDto dto);
        Task<ResponseResult> Delete(long id);
    }
}
