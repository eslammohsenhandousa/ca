﻿using GACA.CORE.Dtos.Stakeholder;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IStakeholderService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title);
        Task<ResponseResult> GetData();
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(AddStakeholderDto dto);
        Task<ResponseResult> Update(UpdateStakeholderDto dto);
        Task<ResponseResult> Delete(long id);
    }
}
