﻿using GACA.CORE.Dtos.Media;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IMediaService
    {
        Task<ResponseResult> SaveFilesRelatedToID(AddMediaDto dto);
    }
}
