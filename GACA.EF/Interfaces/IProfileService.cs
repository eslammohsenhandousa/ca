﻿using GACA.CORE.Dtos.Profile;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IProfileService
    {
        Task<ResponseResult> GetMyProfile(int id);
        Task<ResponseResult> UpdateMyProfile(int id,ProfileDto dto);
        Task<ResponseResult> UpdateMyPassword(int id,UpdateMyPasswordDto dto);
    }
}
