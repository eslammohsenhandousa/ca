﻿using GACA.CORE.Dtos.MasterPlanLayer;
using GACA.CORE.Dtos.User;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IMasterPlanLayerService
    {
        Task<ResponseResult> GetAllByParentId(long id);
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(CreateMasterPlanLayerDto dto);
        Task<ResponseResult> Update(UpdateMasterPlanLayerDto dto);
        Task<ResponseResult> Delete(long id);
    }
}
