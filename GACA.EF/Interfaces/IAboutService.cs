﻿using GACA.CORE.Dtos.About;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IAboutService
    {
        Task<ResponseResult> GetData();
        Task<ResponseResult> Update(AboutDto dto);
    }
}
