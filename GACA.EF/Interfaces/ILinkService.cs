﻿using GACA.CORE.Dtos.Link;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface ILinkService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title);
        Task<ResponseResult> GetData();
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(AddLinkDto dto);
        Task<ResponseResult> Update(UpdateLinkDto dto);
        Task<ResponseResult> Delete(long id);
    }
}
