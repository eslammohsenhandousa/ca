﻿using GACA.CORE.Dtos.Auth.ForgetPassword;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IAuthService
    {
        Task<ResponseResult> ValidateEmployeeUserAsync(string email, string password);
        Task<ResponseResult> ForgetPassword(ForgetPasswordDto dto);
        Task<ResponseResult> ForgetPasswordOTP(ForgetPasswordOTPDto dto);
        Task<ResponseResult> ResetPassword(ResetPasswordDto dto);
    }
}
