﻿using GACA.CORE.Dtos.Home;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IHomeService
    {
        Task<ResponseResult> GetData();
        Task<ResponseResult> Update(HomeDto dto);
    }
}
