﻿using GACA.CORE.Dtos.Message;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IMessageService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email, string? Phone, string? Type);
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Delete(long id);
        Task<ResponseResult> Create(AddMessageDto dto);
    }
}
