﻿using GACA.CORE.Dtos.Blog;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IBlogService
    {
        Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title);
        Task<ResponseResult> GetLastNews();
        Task<ResponseResult> GetById(long id);
        Task<ResponseResult> Create(AddBlogDto dto);
        Task<ResponseResult> Update(UpdateBlogDto dto);
        Task<ResponseResult> Delete(long id);
    }
}
