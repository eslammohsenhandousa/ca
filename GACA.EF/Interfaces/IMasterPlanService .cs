﻿using GACA.CORE.Dtos.MasterPlan;
using GACA.EF.Shared;

namespace GACA.EF.Interfaces
{
    public interface IMasterPlanService
    {
        
        Task<ResponseResult> GetMasterPlanAllData();
        Task<ResponseResult> GetData();
        Task<ResponseResult> Update(MasterPlanDto dto);
    }
}
