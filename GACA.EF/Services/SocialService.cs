﻿using GACA.CORE.Dtos.Social;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class SocialService : ISocialService
    {
        protected ApplicationDbContext _context;

        public SocialService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region GetData
        public async Task<ResponseResult> GetData()
        {
            try
            {
                Social result = await _context.Socials.FirstAsync();
                return new ResponseResult(new SocialDto() {
                    FaceBook = result.FaceBook,
                    Twitter = result.Twitter,
                    YouTube = result.YouTube,
                    Instgram = result.Instgram
                }, ApiStatusCodeEnum.OK);
            } 
            catch (Exception ex) 
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            
        }
        #endregion


        #region Update
        public async Task<ResponseResult> Update(SocialDto dto)
        {
            try
            {
                Social result = await _context.Socials.FirstAsync();
                result.FaceBook = dto.FaceBook;
                result.Twitter  = dto.Twitter;
                result.YouTube  = dto.YouTube;
                result.Instgram  = dto.Instgram;
                _context.Socials.Update(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(new SocialDto() {
                    FaceBook = dto.FaceBook,
                    Twitter = dto.Twitter,
                    YouTube = dto.YouTube,
                    Instgram = dto.Instgram
                }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            

        }
        #endregion
    }
}
