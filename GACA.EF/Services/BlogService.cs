﻿using GACA.CORE.Dtos.Blog;
using GACA.CORE.Dtos.User;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class BlogService : IBlogService
    {
        protected ApplicationDbContext _context;

        public BlogService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title)
        {
            var quarable = _context.Blogs.AsQueryable();
            if (!string.IsNullOrEmpty(Title))
                quarable = quarable.Where(x => x.Title.Contains(Title));
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<Blog> PagedResult = await PagedList<Blog>.CreateAsync(quarable, PageNumber, PageSize);

            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<Blog>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
        
        public async Task<ResponseResult> GetLastNews()
        {
            try
            {
                var news = _context.Blogs.Select(item => new BlogDto
                {
                    Id = item.Id,
                    Title = item.Title,
                    Description = item.Description,
                    ImageUrl = item.ImageUrl,
                    DateCreated = item.DateCreated,
                }).OrderByDescending(x => x.Id).Take(6).OrderBy(x => x.Id).ToList();
                return new ResponseResult(news, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }

        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            Blog result = await _context.Blogs.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(new BlogDto { Id = result.Id, Title = result.Title, Description = result.Description, ImageUrl = result.ImageUrl, DateCreated = result.DateCreated }, ApiStatusCodeEnum.OK);
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddBlogDto dto)
        {
            try
            {
                var st = new Blog
                {
                    Title = dto.Title,
                    Description = dto.Description,
                    DateCreated = DateTime.Now,
                };
                var addResult = await _context.Blogs.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult(new BlogDto { Id = addResult.Entity.Id, Title = st.Title, Description = st.Description , DateCreated = st.DateCreated }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UpdateBlogDto dto)
        {
            try
            {
                #region Check If Record Exist
                Blog record = await _context.Blogs.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (record == null)
                    return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Save To DB Using EntityFrameWork
                record.Title = dto.Title;
                record.Description = dto.Description;
                _context.Blogs.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                Blog result = await _context.Blogs.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
                _context.Blogs.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult("", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        public Task<ResponseResult> Update(BlogDto dto)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
