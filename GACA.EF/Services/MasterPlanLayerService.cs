﻿using GACA.CORE.Dtos.Blog;
using GACA.CORE.Dtos.MasterPlanLayer;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class MasterPlanLayerService : IMasterPlanLayerService
    {
        protected ApplicationDbContext _context;

        public MasterPlanLayerService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get    
        public async Task<ResponseResult> GetAllByParentId(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            var result = await _context.MasterPlanLayers.Where(x => x.MasterPlanId == id).ToListAsync();
            if (result == null)
                return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(result, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            var result = await _context.MasterPlanLayers.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(result, ApiStatusCodeEnum.OK);
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(CreateMasterPlanLayerDto dto)
        {
            try
            {
                var st = new MasterPlanLayer
                {
                    Title = dto.Title,
                    Description = dto.Description,
                    DateCreated = DateTime.Now,
                    MasterPlanId = dto.MasterPlanId,
                };
                var addResult = await _context.MasterPlanLayers.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult(new MasterPlanLayerDto { Id = addResult.Entity.Id, Title = st.Title, Description = st.Description, DateCreated = st.DateCreated }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UpdateMasterPlanLayerDto dto)
        {
            try
            {
                #region Check If Record Exist
                MasterPlanLayer record = await _context.MasterPlanLayers.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (record == null)
                    return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Save To DB Using EntityFrameWork
                record.Title = dto.Title;
                record.Description = dto.Description;
                record.DateUpdated = DateTime.Now;
                _context.MasterPlanLayers.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                MasterPlanLayer result = await _context.MasterPlanLayers.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
                _context.MasterPlanLayers.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult("", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
