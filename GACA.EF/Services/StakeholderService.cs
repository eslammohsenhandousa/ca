﻿using GACA.CORE.Dtos.Stakeholder;
using GACA.CORE.Dtos.User;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class StakeholderService : IStakeholderService
    {
        protected ApplicationDbContext _context;

        public StakeholderService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title)
        {
            var quarable = _context.Stakeholders.AsQueryable();
            if (!string.IsNullOrEmpty(Title))
                quarable = quarable.Where(x => x.Title.Contains(Title));
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<Stakeholder> PagedResult = await PagedList<Stakeholder>.CreateAsync(quarable, PageNumber, PageSize);

            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<Stakeholder>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            Stakeholder result = await _context.Stakeholders.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.StakeholderNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(new StakeholderDto { Id = result.Id, Title = result.Title, DateCreated = result.DateCreated }, ApiStatusCodeEnum.OK);
        }
        
        public async Task<ResponseResult> GetData()
        {
            try
            {
                var result = await _context.Stakeholders.ToListAsync();
                return new ResponseResult(result, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddStakeholderDto dto)
        {
            try
            {
                var st = new Stakeholder
                {
                    Title = dto.Title,
                    DateCreated = DateTime.Now,
                };
                var addResult = await _context.Stakeholders.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult(new StakeholderDto { Id = addResult.Entity.Id, Title = st.Title, DateCreated = st.DateCreated }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UpdateStakeholderDto dto)
        {
            try
            {
                #region Check If Record Exist
                Stakeholder record = await _context.Stakeholders.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (record == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Save To DB Using EntityFrameWork
                record.Title = dto.Title;
                _context.Stakeholders.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                Stakeholder result = await _context.Stakeholders.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                _context.Stakeholders.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult("", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
