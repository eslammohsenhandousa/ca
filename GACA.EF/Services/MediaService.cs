﻿using GACA.CORE.Dtos.Media;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class MediaService : IMediaService
    {
        protected ApplicationDbContext _context;

        public MediaService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Create
        public async Task<ResponseResult> SaveFilesRelatedToID(AddMediaDto dto)
        {
            try
            {
                if(dto.MediaType == "blog")
                {
                    #region Check If Record Exist
                    Blog record = await _context.Blogs.FirstOrDefaultAsync(x => x.Id == dto.Id);
                    if (record == null)
                        return new ResponseResult(Messages.BlogNotFound, ApiStatusCodeEnum.NotFound);
                    #endregion
                    #region Save To DB Using EntityFrameWork
                    record.ImageUrl = dto.MediaUrl;
                    _context.Blogs.Update(record);
                    await _context.SaveChangesAsync();
                    #endregion
                }
                else if(dto.MediaType == "ducoment")
                {
                    #region Check If Record Exist
                    Document record = await _context.Documents.FirstOrDefaultAsync(x => x.Id == dto.Id);
                    if (record == null)
                        return new ResponseResult(Messages.DocumentNotFound, ApiStatusCodeEnum.NotFound);
                    #endregion
                    #region Save To DB Using EntityFrameWork
                    record.ImageUrl = dto.MediaUrl;
                    _context.Documents.Update(record);
                    await _context.SaveChangesAsync();
                    #endregion
                }
                else if (dto.MediaType == "stakeholder")
                {
                    #region Check If Record Exist
                    Stakeholder record = await _context.Stakeholders.FirstOrDefaultAsync(x => x.Id == dto.Id);
                    if (record == null)
                        return new ResponseResult(Messages.StakeholderNotFound, ApiStatusCodeEnum.NotFound);
                    #endregion
                    #region Save To DB Using EntityFrameWork
                    record.ImageUrl = dto.MediaUrl;
                    _context.Stakeholders.Update(record);
                    await _context.SaveChangesAsync();
                    #endregion
                }
                return new ResponseResult("", ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
