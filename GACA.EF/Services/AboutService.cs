﻿using GACA.CORE.Dtos.About;
using GACA.CORE.Dtos.Home;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class AboutService : IAboutService
    {
        protected ApplicationDbContext _context;

        public AboutService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region GetData
        public async Task<ResponseResult> GetData()
        {
            try
            {
                About result = await _context.About.FirstAsync();
                return new ResponseResult(new AboutDto() { Content = result.Content, Vision = result.Vision, Mission = result.Mission }, ApiStatusCodeEnum.OK);
            } 
            catch (Exception ex) 
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            
        }
        #endregion


        #region Update
        public async Task<ResponseResult> Update(AboutDto dto)
        {
            try
            {
                About result = await _context.About.FirstAsync();
                result.Content = dto.Content;
                result.Vision = dto.Vision;
                result.Mission = dto.Mission;
                _context.About.Update(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(new AboutDto() { Content = dto.Content, Vision = dto.Vision, Mission = dto.Mission }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            

        }
        #endregion
    }
}
