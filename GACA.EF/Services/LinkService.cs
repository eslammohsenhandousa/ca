﻿using GACA.CORE.Dtos.Link;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class LinkService : ILinkService
    {
        protected ApplicationDbContext _context;

        public LinkService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title)
        {
            var quarable = _context.Links.AsQueryable();
            if (!string.IsNullOrEmpty(Title))
                quarable = quarable.Where(x => x.Title.Contains(Title));
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<Link> PagedResult = await PagedList<Link>.CreateAsync(quarable, PageNumber, PageSize);

            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<Link>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
       
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            Link result = await _context.Links.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.StakeholderNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(new LinkDto { Id = result.Id, Title = result.Title, LinkAddress = result.LinkAddress , DateCreated = result.DateCreated }, ApiStatusCodeEnum.OK);
        }
        
        public async Task<ResponseResult> GetData()
        {
            try
            {
                var result = await _context.Links.ToListAsync();
                return new ResponseResult(result, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddLinkDto dto)
        {
            try
            {
                var st = new Link
                {
                    Title = dto.Title,
                    LinkAddress = dto.LinkAddress,
                    DateCreated = DateTime.Now,
                };
                var addResult = await _context.Links.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult(new LinkDto { Id = addResult.Entity.Id, Title = st.Title, LinkAddress = dto.LinkAddress , DateCreated = st.DateCreated }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UpdateLinkDto dto)
        {
            try
            {
                #region Check If Record Exist
                Link record = await _context.Links.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (record == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Save To DB Using EntityFrameWork
                record.Title = dto.Title;
                record.LinkAddress = dto.LinkAddress;
                _context.Links.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                Link result = await _context.Links.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                _context.Links.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult("", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
