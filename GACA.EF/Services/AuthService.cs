﻿using GACA.CORE.Dtos.Auth.ForgetPassword;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Security.Cryptography;
using static Humanizer.In;

namespace GACA.EF.Services
{
    public class AuthService : IAuthService
    {
        protected ApplicationDbContext _context;

        private readonly IConfiguration _configuration;

        public AuthService(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }


        #region Login Area
        public async Task<ResponseResult> ValidateEmployeeUserAsync(string email, string password)
        {
            #region Validate
            User user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);

            var result = Auth.VerifyPasswordHash(password, user.Password, user.PasswordSalt);
            if (!result)
                return new ResponseResult(Messages.WrongPassword, ApiStatusCodeEnum.BadRequest);
            #endregion

            #region Generate Token
            string token = await CreateToken(user);
            return new ResponseResult(new { token = token }, ApiStatusCodeEnum.OK);
            #endregion
        }
        #endregion

        #region Forget Password And Reset Password 
        public async Task<ResponseResult> ForgetPassword(ForgetPasswordDto dto)
        {
            #region Validate
            User user = await _context.Users.FirstOrDefaultAsync(x => x.Email == dto.EmailAddress);
            if (user == null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
            #endregion

            #region Generate Token
            string token = SHA256(user.Email + "@" + new Random());
            #endregion
            #region Save User Token
            user.Token = token;
            user.OTP = "1234";
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            #endregion
            return new ResponseResult(new { token = token }, ApiStatusCodeEnum.OK);
        }
        
        public async Task<ResponseResult> ForgetPasswordOTP(ForgetPasswordOTPDto dto)
        {
            #region Validate
            User user = await _context.Users.FirstOrDefaultAsync(x => x.Token == dto.Token);
            if (user == null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
            #endregion

            if(user.OTP != dto.OTP)
            {
                return new ResponseResult(Messages.OTPNotTrue, ApiStatusCodeEnum.BadRequest);
            }

            #region Generate Token
            string token = SHA256(user.Email+"@"+new Random());
            #endregion
            #region Save User Token
            user.Token = token;
            user.OTP = "4321";
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            #endregion

            return new ResponseResult(new { token = token }, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> ResetPassword(ResetPasswordDto dto)
        {
            #region Validate
            User user = await _context.Users.FirstOrDefaultAsync(x => x.Token == dto.Token);
            if (user == null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
            #endregion

            #region Check Password
            if (dto.NewPassword != dto.ConfirmPassword)
                return new ResponseResult(Messages.WrongConfirmPassword, ApiStatusCodeEnum.BadRequest);
            #endregion

            #region Update Password
            byte[] PasswordHashed, PasswordSalt;
            Auth.GetPasswordHash(dto.NewPassword, out PasswordHashed, out PasswordSalt);
            user.Password = PasswordHashed;
            user.PasswordSalt = PasswordSalt;
            user.Token = SHA256(user.Email + "@" + new Random());
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            #endregion

            return new ResponseResult("", ApiStatusCodeEnum.OK);
        }
        #endregion


        #region Private Functions
        private async Task<string> CreateToken(User user)
        {
            string jwtKey = _configuration.GetSection("JWT:Token").Value;
            DateTime expires = DateTime.Now.AddMinutes(8 * 60);
            List<Claim> claims = new List<Claim>
            {
            new Claim("Name", user.Name),
            new Claim("Email", user.Email),
            new Claim("Id", user.Id.ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(claims: claims, expires: expires, signingCredentials: signIn);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private static string SHA256(string s)
        {
            SHA256Managed sha256 = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] hashArray = sha256.ComputeHash(Encoding.UTF8.GetBytes(s));
            foreach (byte b in hashArray)
            {
                hash.Append(b.ToString("x"));
            }
            return hash.ToString();
        }
        #endregion
    }
}
