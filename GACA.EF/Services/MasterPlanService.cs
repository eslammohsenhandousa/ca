﻿using GACA.CORE.Dtos.MasterPlan;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Humanizer;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class MasterPlanService : IMasterPlanService
    {
        protected ApplicationDbContext _context;

        public MasterPlanService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region GetMasterPlanAllData
        public async Task<ResponseResult> GetMasterPlanAllData()
        {
            try
            {
                var result = await _context.MasterPlans.Include(item => item.MasterPlanLayers).ToListAsync();
                return new ResponseResult(result, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }
        #endregion

        #region GetData
        public async Task<ResponseResult> GetData()
        {
            try
            {
                var  result = await _context.MasterPlans.Select(item => new MasterPlanDto
                {
                    Id = item.Id,
                    Title = item.Title,
                    Descripton = item.Descripton,
                }).ToListAsync();
                return new ResponseResult(result, ApiStatusCodeEnum.OK);
            } 
            catch (Exception ex) 
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            
        }
        #endregion


        #region Update
        public async Task<ResponseResult> Update(MasterPlanDto dto)
        {
            try
            {
                MasterPlan result = await _context.MasterPlans.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (result == null)
                    return new ResponseResult(Messages.MasterPlanNotFound, ApiStatusCodeEnum.NotFound);
                result.Title = dto.Title;
                result.Descripton = dto.Descripton;
                _context.MasterPlans.Update(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(new MasterPlan() { Id = dto.Id , Title = dto.Title, Descripton = dto.Descripton }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            

        }
        #endregion
    }
}
