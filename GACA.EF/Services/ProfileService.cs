﻿using GACA.CORE.Dtos.Profile;
using GACA.CORE.Dtos.User;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class ProfileService : IProfileService
    {
        protected ApplicationDbContext _context;

        public ProfileService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseResult> GetMyProfile(int id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                User user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                if (user == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);

                return new ResponseResult(new UserDtos { Id = user.Id, Name = user.Name, Email = user.Email }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }

        public async Task<ResponseResult> UpdateMyPassword(int id,UpdateMyPasswordDto dto)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);

                #region Check User Found 
                User user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                if (user == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Check Current Password
                var result = Auth.VerifyPasswordHash(dto.OldPassword, user.Password, user.PasswordSalt);
                if (!result)
                    return new ResponseResult(Messages.WrongPassword, ApiStatusCodeEnum.BadRequest);
                #endregion

                #region Check Password
                if (dto.NewPassword != dto.ConfirmPassword)
                    return new ResponseResult(Messages.WrongConfirmPassword, ApiStatusCodeEnum.BadRequest);
                #endregion

                #region Update Password
                byte[] PasswordHashed, PasswordSalt;
                Auth.GetPasswordHash(dto.NewPassword, out PasswordHashed, out PasswordSalt);
                user.Password = PasswordHashed;
                user.PasswordSalt = PasswordSalt;

                _context.Users.Update(user);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(new UserDtos { Id = user.Id, Name = user.Name, Email = user.Email }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }

        public async Task<ResponseResult> UpdateMyProfile(int id,ProfileDto dto)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                
                User user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                if (user == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);

                #region Update Data
                user.Name = dto.Name;
                user.Email = dto.Email;

                _context.Users.Update(user);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(new UserDtos { Id = user.Id, Name = dto.Name, Email = dto.Email }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
        }
    }
}
