﻿using GACA.CORE.Dtos.Blog;
using GACA.CORE.Dtos.Message;
using GACA.CORE.Dtos.Stakeholder;
using GACA.CORE.Dtos.User;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GACA.EF.Services
{
    public class MessageService : IMessageService
    {
        protected ApplicationDbContext _context;

        public MessageService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email, string? Phone, string? Type)
        {
            var quarable = _context.Messages.AsQueryable();
            if (!string.IsNullOrEmpty(Name))
                quarable = quarable.Where(x => x.Name.Contains(Name));
            if (!string.IsNullOrEmpty(Phone))
                quarable = quarable.Where(x => x.Phone == Phone);
            if (!string.IsNullOrEmpty(Email))
                quarable = quarable.Where(x => x.Email == Email);
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<Message> PagedResult = await PagedList<Message>.CreateAsync(quarable, PageNumber, PageSize);

            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<Message>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            Message result = await _context.Messages.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.MessageNotFound, ApiStatusCodeEnum.NotFound);
            if (result.IsSeen == false)
            {
                result.IsSeen = true;
                _context.Messages.Update(result);
                await _context.SaveChangesAsync();
            }            
            return new ResponseResult(result, ApiStatusCodeEnum.OK);
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddMessageDto dto)
        {
            try
            {
                string HostName = Dns.GetHostName();
                IPAddress[] ipaddress = Dns.GetHostAddresses(HostName);
                var st = new Message
                {
                    Name = dto.Name,
                    Email = dto.Email,
                    Phone = dto.Phone,
                    FeedBackType = dto.FeedBackType,
                    FeedBackTitle = dto.FeedBackTitle,
                    FeedBackMessage = dto.FeedBackMessage,
                    IsSeen = false,
                    IpAddress = ipaddress[1].ToString(),
                    DateCreated = DateTime.Now,
                };
                var addResult = await _context.Messages.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult("Your Message Has Been Send", ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                Message result = await _context.Messages.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                _context.Messages.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult("", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
