﻿using GACA.CORE.Dtos.Document;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class DocumentService : IDocumentService
    {
        protected ApplicationDbContext _context;

        public DocumentService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Title)
        {
            var quarable = _context.Documents.AsQueryable();
            if (!string.IsNullOrEmpty(Title))
                quarable = quarable.Where(x => x.Title.Contains(Title));
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<Document> PagedResult = await PagedList<Document>.CreateAsync(quarable, PageNumber, PageSize);

            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<Document>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            Document result = await _context.Documents.FirstOrDefaultAsync(x => x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.DocumentNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(new DocumentDto { Id = result.Id, Title = result.Title, DateCreated = result.DateCreated }, ApiStatusCodeEnum.OK);
        }

        public async Task<ResponseResult> GetData()
        {
            var result = await _context.Documents.ToListAsync();
            return new ResponseResult(result, ApiStatusCodeEnum.OK);
        }
        
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddDocumentDto dto)
        {
            try
            {
                var st = new Document
                {
                    Title = dto.Title,
                    DateCreated = DateTime.Now,
                };
                var addResult = await _context.Documents.AddAsync(st);
                await _context.SaveChangesAsync();
                return new ResponseResult(new DocumentDto { Id = addResult.Entity.Id, Title = st.Title, DateCreated = st.DateCreated }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UpdateDocumentDto dto)
        {
            try
            {
                #region Check If Record Exist
                Document record = await _context.Documents.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (record == null)
                    return new ResponseResult(Messages.DocumentNotFound, ApiStatusCodeEnum.NotFound);
                #endregion

                #region Save To DB Using EntityFrameWork
                record.Title = dto.Title;
                _context.Documents.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }

        #endregion

        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                Document result = await _context.Documents.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.DocumentNotFound, ApiStatusCodeEnum.NotFound);
                _context.Documents.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(errorMessage: "", ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion
    }
}
