﻿using GACA.CORE.Dtos.Home;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.Shared;
using Microsoft.EntityFrameworkCore;

namespace GACA.EF.Services
{
    public class HomeService : IHomeService
    {
        protected ApplicationDbContext _context;

        public HomeService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region GetData
        public async Task<ResponseResult> GetData()
        {
            try
            {
                Home result = await _context.Home.FirstAsync();
                return new ResponseResult(new HomeDto() { Title = result.Title, Description = result.Description }, ApiStatusCodeEnum.OK);
            } 
            catch (Exception ex) 
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            
        }
        #endregion


        #region Update
        public async Task<ResponseResult> Update(HomeDto dto)
        {
            try
            {
                Home result = await _context.Home.FirstAsync();
                result.Title = dto.Title;
                result.Description = dto.Description;
                _context.Home.Update(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(new HomeDto() { Title = dto.Title, Description = dto.Description}, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            

        }
        #endregion
    }
}
