﻿using GACA.CORE.Dtos.User;
using GACA.CORE.Models;
using GACA.EF.Enum;
using GACA.EF.Interfaces;
using GACA.EF.ResourceFiles;
using GACA.EF.Shared;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GACA.EF.Services
{
    public class UserService: IUserService
    {
        protected ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Get
        public async Task<ResponseResult> GetAllPagination(int PageNumber, int PageSize, DateTime? CreateAtFrom, DateTime? CreateAtTo, string? Name, string? Email)
        {
            var quarable = _context.Users.Select(item=>new UserWithCreatedDateDtos
            {
                Id = item.Id,
                Name = item.Name,
                Email = item.Email,
                DateCreated = item.DateCreated,
            }).AsQueryable();
            if (!string.IsNullOrEmpty(Name))
                quarable = quarable.Where(x=>x.Name.Contains(Name));
            if (!string.IsNullOrEmpty(Email))
                quarable = quarable.Where(x => x.Email == Email);
            if (CreateAtFrom != null & CreateAtFrom > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated >= CreateAtFrom);
            if (CreateAtTo != null & CreateAtTo > DateTime.MinValue)
                quarable = quarable.Where(x => x.DateCreated <= CreateAtTo);

            PaginationResponseDto pagedList;
            PagedList<UserWithCreatedDateDtos> PagedResult = await PagedList<UserWithCreatedDateDtos>.CreateAsync(quarable, PageNumber, PageSize);
           
            if (PagedResult.Any())
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = PagedResult, TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            else
                pagedList = new PaginationResponseDto { PageNumber = PageNumber, PageSize = PageSize, Data = new List<User>(), TotalItemCount = PagedResult.TotalCount, TotalPagesCount = PagedResult.TotalPages };
            return new ResponseResult(pagedList, ApiStatusCodeEnum.OK);
        }
        public async Task<ResponseResult> GetById(long id)
        {
            if (id <= 0)
                return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
            User result = await _context.Users.FirstOrDefaultAsync(x=>x.Id == id);
            if (result == null)
                return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
            return new ResponseResult(new UserDtos { Email = result.Email,Id=result.Id,Name = result.Name}, ApiStatusCodeEnum.OK);
        }
        #endregion

        #region Create
        public async Task<ResponseResult> Create(AddUserDto dto)
        {
            try
            {
                #region Checks Area
                ResponseResult checkResult = await AddChecks(dto);
                if (!checkResult.IsSucceeded)
                    return checkResult;
                #endregion

                #region Save To DB Using EntityFrameWork
                byte[] PasswordHashed, PasswordSalt;
                Auth.GetPasswordHash(dto.Password, out PasswordHashed, out PasswordSalt);

                var entity = new User
                {
                    Name = dto.Name,
                    Email = dto.Email,
                    Password = PasswordHashed,
                    PasswordSalt = PasswordSalt,
                    DateCreated = DateTime.Now
                };
                var addResult = await _context.Users.AddAsync(entity);
                await _context.SaveChangesAsync();
                #endregion

                #region SendEmail
                // SendMailService.SendMail(dto.Email,"New Account","Welcome New User",false);
                #endregion

                return new ResponseResult(new UserDtos { Email = dto.Email, Id = addResult.Entity.Id, Name = dto.Name }, ApiStatusCodeEnum.Created);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
           
        }
        #endregion

        #region Update
        public async Task<ResponseResult> Update(UserDtos dto)
        {
            try
            {
                #region Checks Area
                ResponseResult checkResult = await UpdateChecks(dto);
                if (!checkResult.IsSucceeded)
                    return checkResult;
                #endregion

                #region Save To DB Using EntityFrameWork
                User record = (User)checkResult.ReturnData;
                record.Name = dto.Name;
                record.Email = dto.Email;
                _context.Users.Update(record);
                await _context.SaveChangesAsync();
                #endregion

                return new ResponseResult(dto, ApiStatusCodeEnum.OK);
            } catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }
            
        }

        #endregion

        #region ChangePassword
        public async Task<ResponseResult> ChangePassword(ChangePasswordDto dto)
        {
            try
            {
                #region Checks Area
                User isEmailExist = await _context.Users.FirstOrDefaultAsync(x => x.Email == dto.Email );
                if (isEmailExist == null)
                    return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
                if (dto.NewPassword != dto.ConfirmPassword)
                    return new ResponseResult(Messages.WrongConfirmPassword, ApiStatusCodeEnum.BadRequest);
                var result = Auth.VerifyPasswordHash(dto.OldPassword, isEmailExist.Password, isEmailExist.PasswordSalt);
                if (!result)
                    return new ResponseResult(Messages.WrongPassword, ApiStatusCodeEnum.BadRequest);
                #endregion

                #region Update Password
                byte[] PasswordHashed, PasswordSalt;
                Auth.GetPasswordHash(dto.NewPassword, out PasswordHashed, out PasswordSalt);
                isEmailExist.Password = PasswordHashed;
                isEmailExist.PasswordSalt = PasswordSalt;
                _context.Users.Update(isEmailExist);
                await _context.SaveChangesAsync();
                #endregion
                return new ResponseResult(true, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion


        #region UpdatePassword
        public async Task<ResponseResult> UpdatePassword(UpdatePasswordDto dto)
        {
            try
            {
                #region Checks Area
                User isUserExist = await _context.Users.FirstOrDefaultAsync(x => x.Id == dto.UserId);
                if (isUserExist == null)
                    return new ResponseResult(Messages.UserAlreadyExist, ApiStatusCodeEnum.BadRequest);
                if (dto.NewPassword != dto.ConfirmPassword)
                    return new ResponseResult(Messages.WrongConfirmPassword, ApiStatusCodeEnum.BadRequest);
                #endregion

                #region Update Password
                byte[] PasswordHashed, PasswordSalt;
                Auth.GetPasswordHash(dto.NewPassword, out PasswordHashed, out PasswordSalt);
                isUserExist.Password = PasswordHashed;
                isUserExist.PasswordSalt = PasswordSalt;
                _context.Users.Update(isUserExist);
                await _context.SaveChangesAsync();
                #endregion
                return new ResponseResult(true, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion


        #region Delete
        public async Task<ResponseResult> Delete(long id)
        {
            try
            {
                if (id <= 0)
                    return new ResponseResult(Messages.DataNotValid, ApiStatusCodeEnum.BadRequest);
                User result = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                if (result == null)
                    return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
                _context.Users.Remove(result);
                await _context.SaveChangesAsync();
                return new ResponseResult(new UserDtos { Email = result.Email, Id = result.Id, Name = result.Name }, ApiStatusCodeEnum.OK);
            }
            catch (Exception ex)
            {
                return new ResponseResult(ex.Message, ApiStatusCodeEnum.InternalServerError);
            }

        }
        #endregion

        #region Private Functions
        private async Task<ResponseResult> AddChecks(AddUserDto dto)
        {

            #region Check Name Already Exists
            User isEmailExist = await _context.Users.FirstOrDefaultAsync(x => x.Email == dto.Email);
            if (isEmailExist != null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
            #endregion

            return new ResponseResult(true);
        }
        private async Task<ResponseResult> UpdateChecks(UserDtos dto)
        {

            #region Check If Record Exist
            User record = await _context.Users.FirstOrDefaultAsync(x => x.Id == dto.Id);
            if (record == null)
                return new ResponseResult(Messages.UserNotFound, ApiStatusCodeEnum.NotFound);
            #endregion

            #region Check Name Already Exists
            User isEmailExist = await _context.Users.FirstOrDefaultAsync(x => x.Email == dto.Email && x.Id != dto.Id);
            if (isEmailExist != null)
                return new ResponseResult(Messages.EmailAlreadyExist, ApiStatusCodeEnum.BadRequest);
            #endregion

            return new ResponseResult(record, ApiStatusCodeEnum.OK);
        }
        #endregion
    }
}
