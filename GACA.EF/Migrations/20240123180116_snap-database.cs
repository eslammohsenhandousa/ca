﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GACA.EF.Migrations
{
    public partial class snapdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "About",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Vision = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Mission = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_About", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Blogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Home",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Home", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Links",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    LinkAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Links", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MaterPlans",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Descripton = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterPlans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FeedBackType = table.Column<int>(type: "int", nullable: false),
                    FeedBackTitle = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    FeedBackMessage = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsSeen = table.Column<bool>(type: "bit", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Socials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FaceBook = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Twitter = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    YouTube = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Instgram = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socials", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stakeholders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stakeholders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<byte[]>(type: "varbinary(150)", maxLength: 150, nullable: false),
                    PasswordSalt = table.Column<byte[]>(type: "varbinary(max)", nullable: false),
                    OTP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterPlanLayers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    MasterPlanId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterPlanLayers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterPlanLayers_MaterPlans_MasterPlanId",
                        column: x => x.MasterPlanId,
                        principalTable: "MaterPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "About",
                columns: new[] { "Id", "Content", "Mission", "Vision" },
                values: new object[] { 1, "Welcome Content LookUp", "Welcome Mission LookUp", "Welcome Vision LookUp" });

            migrationBuilder.InsertData(
                table: "Home",
                columns: new[] { "Id", "Description", "Title" },
                values: new object[] { 1, "Welcome Description LookUp", "Welcome Title LookUp" });

            migrationBuilder.InsertData(
                table: "MaterPlans",
                columns: new[] { "Id", "Descripton", "Title" },
                values: new object[,]
                {
                    { 1, "", "Layer 1" },
                    { 2, "", "Layer 2" },
                    { 3, "", "Layer 3" },
                    { 4, "", "Layer 4" },
                    { 5, "", "Layer 5" }
                });

            migrationBuilder.InsertData(
                table: "Socials",
                columns: new[] { "Id", "FaceBook", "Instgram", "Twitter", "YouTube" },
                values: new object[] { 1, "https://facebook.com", "https://instagram.com", "https://twitter.com", "https://youtube.com" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DateCreated", "DateUpdated", "Email", "Name", "OTP", "Password", "PasswordSalt", "Token" },
                values: new object[] { 1, new DateTime(2024, 1, 23, 21, 1, 16, 254, DateTimeKind.Local).AddTicks(8844), new DateTime(2024, 1, 23, 21, 1, 16, 254, DateTimeKind.Local).AddTicks(8854), "admin@admin.com", "Admin", "", new byte[] { 117, 83, 77, 28, 241, 154, 34, 42, 61, 231, 158, 168, 169, 175, 127, 242, 236, 61, 90, 28, 199, 161, 27, 79, 156, 97, 213, 166, 95, 173, 158, 44, 79, 27, 102, 87, 2, 122, 96, 71, 143, 103, 193, 175, 176, 213, 85, 225, 171, 210, 111, 74, 82, 139, 83, 189, 255, 119, 120, 128, 23, 250, 89, 239 }, new byte[] { 107, 41, 78, 246, 127, 206, 46, 191, 118, 87, 107, 138, 63, 235, 255, 230, 80, 251, 169, 211, 97, 156, 95, 155, 82, 238, 90, 122, 103, 72, 75, 244, 48, 255, 56, 70, 210, 134, 101, 85, 56, 123, 189, 172, 25, 184, 99, 45, 93, 69, 153, 169, 60, 63, 133, 209, 84, 177, 182, 143, 187, 26, 171, 163, 188, 99, 110, 147, 55, 201, 181, 107, 227, 14, 65, 159, 209, 244, 49, 63, 37, 3, 64, 192, 164, 4, 222, 119, 122, 214, 201, 155, 11, 49, 170, 147, 17, 22, 220, 62, 249, 80, 4, 202, 4, 96, 141, 161, 75, 202, 128, 82, 18, 58, 199, 88, 244, 14, 34, 175, 61, 55, 252, 162, 146, 68, 137, 88 }, "" });

            migrationBuilder.CreateIndex(
                name: "IX_MasterPlanLayers_MasterPlanId",
                table: "MasterPlanLayers",
                column: "MasterPlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "About");

            migrationBuilder.DropTable(
                name: "Blogs");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "Home");

            migrationBuilder.DropTable(
                name: "Links");

            migrationBuilder.DropTable(
                name: "MasterPlanLayers");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "Socials");

            migrationBuilder.DropTable(
                name: "Stakeholders");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "MaterPlans");
        }
    }
}
