﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Auth
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Email Addres is required"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Invalid email address.")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
