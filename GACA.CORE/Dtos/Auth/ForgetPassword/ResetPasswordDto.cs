﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Auth.ForgetPassword
{
    public class ResetPasswordDto
    {
        [Required(ErrorMessage = "Token is required")]
        [Display(Name = "Token")]
        public string Token { get; set; }

        [Required, MaxLength(150), DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Required, MaxLength(150), DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }
}
