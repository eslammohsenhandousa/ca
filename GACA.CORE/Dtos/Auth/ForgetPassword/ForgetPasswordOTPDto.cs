﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Auth.ForgetPassword
{
    public class ForgetPasswordOTPDto
    {
        [Required(ErrorMessage = "Token is required")]
        [Display(Name = "Token")]
        public string Token { get; set; }

        [Required(ErrorMessage = "OTP is required")]
        [Display(Name = "OTP")]
        public string OTP { get; set; }
    }
}
