﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Landing
{
    public class LandingDto
    {
        public object? Hero { get; set; }

        public object? MasterPlan { get; set; }

        public object? News { get; set; }

        public object? StakeHolder { get; set; }
    }
}
