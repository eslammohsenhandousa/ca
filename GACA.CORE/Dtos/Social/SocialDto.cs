﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Social
{
    public class SocialDto
    {
        [Required(ErrorMessage = "FaceBook Url is required"), DataType(DataType.Url), Url(ErrorMessage = "Invalid Facebook Url.")]
        [Display(Name = "FaceBook Url")]
        public string FaceBook { get; set; } = string.Empty;

        [Required(ErrorMessage = "Twitter Url is required"), DataType(DataType.Url), Url(ErrorMessage = "Invalid Twitter Url.")]
        [Display(Name = "Twitter Url")]
        public string Twitter { get; set; } = string.Empty;

        [Required(ErrorMessage = "YouTube Url is required"), DataType(DataType.Url), Url(ErrorMessage = "Invalid YouTube Url.")]
        [Display(Name = "YouTube Url")]
        public string YouTube { get; set; } = string.Empty;

        [Required(ErrorMessage = "Instgram Url is required"), DataType(DataType.Url), Url(ErrorMessage = "Invalid Instgram Url.")]
        [Display(Name = "Instgram Url")]
        public string Instgram { get; set; } = string.Empty;
    }
}
