﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Link
{
    public class LinkDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required"), MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required(ErrorMessage = "Link Address is required"), DataType(DataType.Url), Url(ErrorMessage = "Invalid Link Address.")]
        [Display(Name = "Link Address")]
        public string LinkAddress { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
