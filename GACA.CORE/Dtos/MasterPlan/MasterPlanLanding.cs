﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.MasterPlan
{
    public class MasterPlanLanding
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required]
        public string Descripton { get; set; } = string.Empty;

        public string[] Layers { get; set; }
    }
}
