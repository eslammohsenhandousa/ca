﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.MasterPlan
{
    public class MasterPlanDto
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        public string Descripton { get; set; } = string.Empty;
    }
}
