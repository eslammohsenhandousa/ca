﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.About
{
    public class AboutDto
    {
        [Required, DataType(DataType.Text)]
        public string Content { get; set; } = string.Empty;

        [Required, DataType(DataType.Text)]
        public string Vision { get; set; } = string.Empty;

        [Required, DataType(DataType.Text)]
        public string Mission { get; set; } = string.Empty;
    }
}
