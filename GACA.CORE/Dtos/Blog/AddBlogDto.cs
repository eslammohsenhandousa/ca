﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Blog
{
    public class AddBlogDto
    {
        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Html)]
        public string Description { get; set; } = string.Empty;

        [Required, DataType(DataType.ImageUrl)]
        public string? ImageUrl { get; set; } = string.Empty;
    }
}
