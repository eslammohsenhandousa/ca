﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Document
{
    public class AddDocumentDto
    {
        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.ImageUrl)]
        public string? ImageUrl { get; set; } = string.Empty;
    }
}
