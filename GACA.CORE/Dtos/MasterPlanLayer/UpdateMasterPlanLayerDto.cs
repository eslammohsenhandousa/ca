﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.MasterPlanLayer
{
    public class UpdateMasterPlanLayerDto
    {
        [Required]
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Html)]
        public string Description { get; set; } = string.Empty;

        [Required]
        public int MasterPlanId { get; set; } = 0;
    }
}
