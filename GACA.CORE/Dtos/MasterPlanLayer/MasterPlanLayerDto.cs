﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.MasterPlanLayer
{
    public class MasterPlanLayerDto
    {
        [Required]
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Html)]
        public string Description { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        public DateTime? DateUpdated { get; set; } = DateTime.Now;
    }
}
