﻿using System.ComponentModel.DataAnnotations;


namespace GACA.CORE.Dtos.Home
{
    public class HomeDto
    {
        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Text)]
        public string Description { get; set; } = string.Empty;
    }
}
