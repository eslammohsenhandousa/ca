﻿using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace GACA.CORE.Dtos.Media
{
    public class AddMediaDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string MediaType { get; set; } = string.Empty;

        [Required, DataType(DataType.Url)]
        public string MediaUrl { get; set; } = string.Empty;
    }
}
