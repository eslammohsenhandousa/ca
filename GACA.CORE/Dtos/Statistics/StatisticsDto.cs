﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GACA.CORE.Dtos.Statistics
{
    public class StatisticsDto
    {
        public int UserCount { get; set; } = 0;

        public int DocumentCount { get; set; } = 0;

        public int BlogCount { get; set; } = 0;

        public int StakeholderCount { get; set; } = 0;

        public int MessageCount { get; set; } = 0;
    }
}
