﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.Settings
{
    public class SettingsDto
    {
        public string Version { get; set; } = string.Empty;
        public string Copyright { get; set; } = string.Empty;
        public string Constructor { get; set; } = string.Empty;
        public string FooterDescription { get; set; } = string.Empty;
        public object? Links { get; set; }
        public object? Social { get; set; }
    }
}
