﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GACA.CORE.Dtos.User
{
    public class UpdatePasswordDto
    {
        [Required]
        public int UserId { get; set; }

        [Required, MaxLength(150), DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }
        
        [Required, MaxLength(150), DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }
}
