﻿using System.ComponentModel.DataAnnotations;

namespace GACA.CORE.Dtos.User
{
    public class UserWithCreatedDateDtos
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "Email Addres is required"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Invalid email address.")]
        [Display(Name = "Email Address")]
        public string Email { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
