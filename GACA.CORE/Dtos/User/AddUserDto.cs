﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GACA.CORE.Dtos.User
{
    public class AddUserDto
    {
        [Required, MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "Email Addres is required"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Invalid email address.")]
        [Display(Name = "Email Address")]
        public string Email { get; set; } = string.Empty;

        [Required, MaxLength(150), DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;
    }
}
