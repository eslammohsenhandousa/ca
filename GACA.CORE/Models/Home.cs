﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("Home")]
    public class Home
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Text)]
        public string Description { get; set; } = string.Empty;
    }
}
