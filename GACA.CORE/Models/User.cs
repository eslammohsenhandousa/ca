﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required"), MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "Email Addres is required"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Invalid email address.")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required"), MaxLength(150), DataType(DataType.Password)]
        public byte[] Password { get; set; }

        public byte[] PasswordSalt { get; set; }

        public string OTP { get; set; } = string.Empty;

        public string Token { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        public DateTime? DateUpdated { get; set; } = DateTime.Now;
    }
}
