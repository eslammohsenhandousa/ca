﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("Messages")]
    public class Message
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required"), MaxLength(150)]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "Email Addres is required"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Invalid email address.")]
        [Display(Name = "Email Address")]
        public string Email { get; set; } = string.Empty;

        [Required(ErrorMessage = "Phone Number is required"), DataType(DataType.PhoneNumber), Phone(ErrorMessage = "Invalid Phone Number")]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; } = string.Empty;

        [Required(ErrorMessage = "Type is required")]
        public FeedBackTypeEnum FeedBackType { get; set; }

        [Required(ErrorMessage = "Title is required"), MaxLength(150)]
        public string FeedBackTitle { get; set; }

        [Required(ErrorMessage = "Message is required"), DataType(DataType.Text)]
        public string FeedBackMessage { get; set; }

        [Required]
        public Boolean IsSeen { get; set; } = false;

        [Required]
        public string IpAddress { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }

    public enum FeedBackTypeEnum
    {
        Recommendation,
        ErrorReport,
        Question,
        Other
    }
}
