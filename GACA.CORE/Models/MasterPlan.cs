﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("MaterPlans")]
    public class MasterPlan
    {
        public MasterPlan() { 
            MasterPlanLayers = new HashSet<MasterPlanLayer>();
        }

        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required]
        public string Descripton { get; set; } = string.Empty;

        public ICollection<MasterPlanLayer> MasterPlanLayers { get; set; } 

    }
}
