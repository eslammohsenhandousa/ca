﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("Documents")]
    public class Document
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
