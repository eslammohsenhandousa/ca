﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("About")]
    public class About
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Content is required"), DataType(DataType.Text)]
        public string Content { get; set; } = string.Empty;

        [Required(ErrorMessage = "Vision is required"), DataType(DataType.Text)]
        public string Vision { get; set; } = string.Empty;

        [Required(ErrorMessage = "Mission is required"), DataType(DataType.Text)]
        public string Mission { get; set; } = string.Empty;
    }
}
