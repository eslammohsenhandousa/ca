﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("Stakeholders")]
    public class Stakeholder
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required"), MaxLength(150)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Image Url is required"), DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        public DateTime? DateUpdated { get; set; } = DateTime.Now;
    }
}
