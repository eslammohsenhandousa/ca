﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GACA.CORE.Models
{
    [Table("MasterPlanLayers")]
    public class MasterPlanLayer
    {
        public int Id { get; set; }

        [Required, MaxLength(150)]
        public string Title { get; set; } = string.Empty;

        [Required, DataType(DataType.Html)]
        public string Description { get; set; } = string.Empty;

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        public DateTime? DateUpdated { get; set; } = DateTime.Now;

        [Required]
        public int MasterPlanId { get; set; } = 0;
    }
}
